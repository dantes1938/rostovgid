import React, { Component } from 'react'
import CarouselBox from '../Components/CarouselBox';
import {Container} from 'react-bootstrap';

export default class Authorized extends Component {
  render() {
    return (
        <>
            <Container>
                <CarouselBox/>
            </Container>
        </>
    )
  }
}
