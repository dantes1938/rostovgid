import React, { Component } from 'react'
import {Col, Figure, Row} from 'react-bootstrap';
import user from '../assets/user.jpg';

export default class More1 extends Component {
  render() {
    return (
      <>
        <h1 className='mt-3'>
          Список участников:        
        </h1>
      <Row xs={1} md={1} className="g-4 m-8">
        <Col md="12">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>User name</h5>
            <p>
              Несколько слов об участнике
            </p>
          </div>
        </div>
        </Col>
        <Col md="12">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>User name</h5>
            <p>
              Несколько слов об участнике
            </p>
          </div>
        </div>
        </Col>
        <Col md="12">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>User name</h5>
            <p>
              Несколько слов об участнике
            </p>
          </div>
        </div>
        </Col>
    </Row>
      </>
    )
  }
}

