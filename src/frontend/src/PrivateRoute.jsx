import React from 'react';
import {Route, Navigate} from 'react-router-dom';
import { withAuth } from './Auth/AuthProvider'


export const PrivateRoute = withAuth(({component: RouteComponent, isAuthorised, ...rest}) => (
    <Route {...rest} render={routeProps => (
        isAuthorised ? <RouteComponent {...routeProps}/> : <Navigate to={"/login"}/>
    )}/>
))
