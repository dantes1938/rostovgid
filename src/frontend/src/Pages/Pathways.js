import React, { Component } from 'react';
import { Card, Col, Container, ListGroup, Row, Button, CardGroup, ButtonGroup, Nav, Tab, TabContainer } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route, Link, NavLink} from 'react-router-dom';
import CarouselBox from '../Components/CarouselBox'
import caption from '../assets/caption.jpg';
import vk from '../assets/vk.jpg';
import wh from '../assets/wh.jpg';
import Pathway1 from '../Routings/Pathway1';
import Pathway2 from '../Routings/Pathway2';
import Pathway3 from '../Routings/Pathway3';
import More1 from '../Routings/More1';
import More2 from '../Routings/More2';
import More3 from '../Routings/More3';
import Newroute from '../Routings/Newroute';


export default class Pathways extends Component {
  render() {
    return (
      //<CarouselBox/>
      <TabContainer>
        <Container>          
        <Row>
        <Col md="8">              
          <h2>Популярные маршруты
            <Button variant="success" className='m-5' as={Link} to="/newroute">Создать совместный поход</Button>
          </h2>
        <CardGroup>            
        <Card style={{ width: '15rem' }} className="m-1">
          <Card.Img variant="top" src={caption} />
            <Card.Body>
              <Card.Title>Название</Card.Title>
                <Card.Text>
                Краткое описание маршрута
                </Card.Text>
                    <Nav variant="pills">
                      <Nav.Item>                      
                        <Nav.Link eventKey="look1" >Посмотреть</Nav.Link>                                          
                      </Nav.Item>
                      <Nav.Item>                       
                        <Nav.Link eventKey="more1" >Подробнее</Nav.Link>        
                      </Nav.Item>
                  </Nav>              
            </Card.Body>
          </Card> 
          <Card style={{ width: '15rem' }} className="m-1">
          <Card.Img variant="top" src={vk} />
            <Card.Body>
              <Card.Title>Название</Card.Title>
                <Card.Text>
                Краткое описание маршрута
                </Card.Text>
                <Nav variant="pills">
                      <Nav.Item>                      
                        <Nav.Link eventKey="look2" >Посмотреть</Nav.Link>                                          
                      </Nav.Item>
                      <Nav.Item>                       
                        <Nav.Link eventKey="more2" >Подробнее</Nav.Link>        
                      </Nav.Item>
                  </Nav>       
            </Card.Body>
          </Card> 
          <Card style={{ width: '15rem' }} className="m-1">
          <Card.Img variant="top" src={wh} />
            <Card.Body>
              <Card.Title>Название</Card.Title>
                <Card.Text>
                Краткое описание маршрута
                </Card.Text>
                <Nav variant="pills">
                      <Nav.Item>                      
                        <Nav.Link eventKey="look3" >Посмотреть</Nav.Link>                                          
                      </Nav.Item>
                      <Nav.Item>                       
                        <Nav.Link eventKey="more3" >Подробнее</Nav.Link>        
                      </Nav.Item>
                  </Nav>       
            </Card.Body>
          </Card> 
        </CardGroup>                             
          </Col>
          <Col md="4">
              <h3 className='text-center'>Совместные походы</h3>
              <Card>
                <ListGroup variant="flush">
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                  <ListGroup.Item>Маршрут: , Кол-во человек: <Button variant="outline-success" className='m-3'>Участвовать</Button>{' '}</ListGroup.Item>
                </ListGroup>
              </Card>
        </Col>
        <Col sm={17}>
                  <Tab.Content className="mt-3">
                    <Tab.Pane eventKey="look1">
                      <h1>Следуя этому маршруту вы сможете посмотреть:</h1>
                      <Pathway1/>
                      <h1 className="mt-3">Описание маршрута:</h1>                                           
                    </Tab.Pane>
                    <Tab.Pane eventKey="more1">
                      <More1/>
                    </Tab.Pane>
                    <Tab.Pane eventKey="look2">
                      <h1>Следуя этому маршруту вы сможете посмотреть:</h1>
                      <Pathway1/>
                      <h1 className="mt-3">Описание маршрута:</h1>                                           
                    </Tab.Pane>
                    <Tab.Pane eventKey="more2">
                      <More1/>
                    </Tab.Pane>
                    <Tab.Pane eventKey="look3">
                      <h1>Следуя этому маршруту вы сможете посмотреть:</h1>
                      <Pathway1/>
                      <h1 className="mt-3">Описание маршрута:</h1>                                           
                    </Tab.Pane>
                    <Tab.Pane eventKey="more3">
                      <More1/>
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
        </Row>
          
      </Container>
    </TabContainer>
    )
  }
}
/*
 <Routes>
            <Route exact path="/pathway1" element={<Pathway1/>} />
            <Route exact path="/pathway2" element={<Pathway2/>} />
            <Route exact path="/pathway3" element={<Pathway3/>} />
            <Route exact path="/more1" element={<More1/>} />
            <Route exact path="/more2" element={<More2/>} />
            <Route exact path="/more3" element={<More3/>} />
            <Route exact path="newroute" element={<Newroute/>} />
          </Routes>   
*/ 