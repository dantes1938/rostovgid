import React, { Component } from 'react';
import { Button, Form, Row, Col, FormCheck } from 'react-bootstrap';
import { Route, Routes } from 'react-router';
import Authorized from '../Routings/Authorized';
import Registr from '../Routings/Registr';
import {Link, NavLink} from 'react-router-dom';

//import Snd from '../assets/Snd.jpg';

const imgMyimageexample = require('../assets/Snd.jpg');
const divStyle = {
  width: '88%',
  height: '800px',
  backgroundImage: 'Snd.jpg',
  backgroundSize: 'cover'  
};

export default class Login extends Component {
  
  render() {
    return (
      
        <div className="cComponent" style={divStyle} >
          <Form>
            <h2 className='text-center'>Авторизация</h2>
          <Row className="mb-3">
            <Form.Group as={Col} controlId="formGridLogin">
              <Form.Label>Login</Form.Label>
              <Form.Control type="login" placeholder="Enter login" />
            </Form.Group>

            <Form.Group as={Col} controlId="formGridPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
            </Form.Group>
          </Row>
          <Form.Group className="mb-3" controlId="FormBasicCheckbox">
                <Form.Check type="checkbox" label="Запомнить меня"/>
          </Form.Group>
            <Button as={Link} to="/Authorized" variant="primary" type="enter">
              Войти
            </Button>
          <h2 className='text-center'>Регистрация</h2>
          <Form.Group className="mb-3" controlId="formGridEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control placeholder="Enter Email" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formGridLogin2">
              <Form.Label>Login</Form.Label>
              <Form.Control placeholder="Enter login" />
            </Form.Group>     

          <Form.Group className="mb-3" controlId="formGridPassword2">
            <Form.Label>Password</Form.Label>
            <Form.Control placeholder="Enter Password" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formGridPassword3">
            <Form.Label>Repeat Password</Form.Label>
            <Form.Control placeholder="Repeat Password" />
          </Form.Group>
          <Button as={Link} to="/registr" variant="primary" type="enter">
            Зарегистрироваться
          </Button>
        </Form>
        <Routes>
          <Route exact path="/registr" element={<Registr/>} />
        </Routes>
      </div>
      
    )
  }
}
