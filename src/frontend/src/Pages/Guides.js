import React, { Component } from 'react';
import { Card, Col, Row} from 'react-bootstrap';
import user from '../assets/user.jpg';

export default class Guides extends Component {
  render() {
    return (
        <Row xs={1} md={2} className="g-4 m-3">
          <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид1</h5>
            <p>
              Несколько слов об участнике
            </p>
          </div>
        </div>
        </Col>
        <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид2</h5>
            <p>
              Описание
            </p>
          </div>
        </div>
        </Col>
        <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид3</h5>
            <p>
            Описание
            </p>
          </div>
        </div>
        </Col>
        <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид4</h5>
            <p>
            Описание
            </p>
          </div>
        </div>
        </Col>
        <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид5</h5>
            <p>
            Описание
            </p>
          </div>
        </div>
        </Col>
        <Col md="4">
        <div class="d-flex align-items-center">
          <div class="flex-shrink-0">
            <img
               width={150}
               height={130}
               alt="150x130"
               src={user}
            />
          </div>
          <div class="flex-grow-2 ms-3">
            <h5>Гид6</h5>
            <p>
            Описание
            </p>
          </div>
        </div>
        </Col>
      </Row>
    )
  }
}

/*

<Col>
            <Card>
              <Card.Img variant="top" width={150} height={130} alt="150x130" src={user} />                       
              <Card.Body>
                <Card.Title>Гид1</Card.Title>
                <Card.Text>
                 Описание
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Img variant="top" src="holder.js/100px160" />
              <Card.Body>
                <Card.Title>Гид2</Card.Title>
                <Card.Text>
                Описание
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Img variant="top" src="holder.js/100px160" />
              <Card.Body>
                <Card.Title>Гид3</Card.Title>
                <Card.Text>
                Описание
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Img variant="top" src="holder.js/100px160" />
              <Card.Body>
                <Card.Title>Гид4</Card.Title>
                <Card.Text>
                Описание
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>       
      </Row>
*/ 