import React, {Component ,useState} from 'react';

//import CarouselBox from '../Components/CarouselBox'
import {Col, Container, Row  ,Card, Button, PopoverHeader, Modal} from 'react-bootstrap';
import { Paper, Typography, Grid, BottomNavigation, BottomNavigationAction } from '@material-ui/core';

//import MenuIcon from '@material-ui/icons/Menu';
import FolderIcon from '@material-ui/icons/Folder';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';

import {makeStyles} from '@material-ui/core/styles';
import rostov from '../assets/rostov.jpg';
import rostov2 from '../assets/rostov2.jpg';
import rostov4 from '../assets/rostov4.jpg';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(1)
    },
    title: {
        flexGrow:1
    },
    mainFeaturesPost: {
        position: "relative",
        color: theme.palette.common.white,
        marginBottom: theme.spacing(4),

        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center"
    },
    overlay: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundOverlay: "rgba(0,0,0,.3)"
    },
    mainFeaturePostContent: {
        position: "relative",
        padding: theme.spacing(9),
    },
})) 

export default function Attractions() {
    const classes = useStyles();
    const [value, setValue] = React.useState("recents")

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const[show , setShow ] = useState(false);
    const handleClose = () =>setShow(false);
    const handleShow = () =>setShow(true);
    
  return (
    <>
    <main>
        <Paper className={classes.mainFeaturesPost} style={{backgroundImage: 'url(https://drevo-info.ru/images/002/003963.jpg)'}}>
            <Container fixed>
                <div className={classes.overlay}/>
                <Grid container>
                    <Grid item md={6}>
                        <div className={classes.mainFeaturePostContent}>
                            <Typography component="h1" variant="h3" color="initial" gutterBottom>
                                Самые узнаваемые места
                            </Typography>
                        </div> 
                    </Grid> 
                </Grid>
            </Container>
        </Paper>
    </main>
        <Container style={{paddingTop: '2rem' , paddingBottom:'2rem'}}>
            <Row>
                <Col>
                    <Card style={{ width: '25rem' }} bg="light" >
                        <Card.Img variant="top" src={rostov} />
                        <Card.Body  >
                            <Card.Title>Городская Дума </Card.Title>
                            <Card.Text >
                            Здание Городской Думы - одна из самых красивых построек в городе и одна из главных архитектурных достопримечательностей города. Изначально в здании разместилась городская дума и управа. В советское время в здании находился областной комитет КПСС.
                            </Card.Text>
                            <Button onClick={handleShow} >Подробнее</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card style={{ width: '25rem' }} bg="light" >
                        <Card.Img variant="top" src={rostov2} />
                        <Card.Body  >
                            <Card.Title>Улица Большая Садовая </Card.Title>
                            <Card.Text >
                            Большая Садовая улица — центральная улица Ростова-на-Дону, одна из старейших и красивейших улиц города, в частности, на Большой Садовой находятся такие достопримечательности как Городской дом, Музыкальный театр, Дом Черновой, ЦУМ.
                            </Card.Text>
                            <Button href="https://ru.wikipedia.org/wiki/%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F_%D0%A1%D0%B0%D0%B4%D0%BE%D0%B2%D0%B0%D1%8F_%D1%83%D0%BB%D0%B8%D1%86%D0%B0_(%D0%A0%D0%BE%D1%81%D1%82%D0%BE%D0%B2-%D0%BD%D0%B0-%D0%94%D0%BE%D0%BD%D1%83)">Подробнее</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card style={{ width: '25rem' }} bg="light" >
                        <Card.Img variant="top" src={rostov4} />
                        <Card.Body  >
                            <Card.Title>Ростов Арена</Card.Title>
                            <Card.Text >
                            Высота футбольной арены - 51 м. Центральным ядром стадиона является футбольное поле с игровой зоной размером 105 х 68 метров. Вместимость стадиона – 45180 зрителей. На стадионе применена уникальная система максимального угла обзора в 32 градуса.
                            </Card.Text>
                            <Button href='https://ru.wikipedia.org/wiki/%D0%A0%D0%BE%D1%81%D1%82%D0%BE%D0%B2_%D0%90%D1%80%D0%B5%D0%BD%D0%B0'>Подробнее</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    <footer>
        <Typography variant='h6' align="center" gutterBottom></Typography>
        <BottomNavigation value={value} onChange={handleChange} className={classes.root}>
            <BottomNavigationAction label="Recents" value="recents" icon={<RestoreIcon/>}/>
            <BottomNavigationAction label="Favourites" value="favourites" icon={<FavoriteIcon/>}/>
            <BottomNavigationAction label="Nearby" value="nearby" icon={<LocationOnIcon/>}/>
            <BottomNavigationAction label="Folder" value="folder" icon={<FolderIcon/>}/>
        </BottomNavigation>
    </footer>
    <Modal  show={show} onHide={handleClose} >
                    <Modal.Header closeButton >
                        <Modal.Title>Городская дума</Modal.Title>
                    </Modal.Header>
                    <Modal.Body  >В середине 1890-х годов администрация Ростова-на-Дону принимает решение о строительстве здания городской думы (ранее дума заседала в доме Максимова)[4]. Для разработки проекта пригласили известного архитектора Александра Никаноровича Померанцева, который уже построил в городе несколько зданий. Окончательный вариант проекта здания был утверждён городским управлением в конце 1896 года. В 1897 году началось строительство городского дома. К 1899 году дом был построен и освящён[5].

По подсчётам архитектора Померанцева, строительство должно было обойтись в 586 176 рублей, 16 копеек. Но в думе сочли, что эта сумма слишком большая и её необходимо ужать. Финансовая комиссия думы утвердила окончательную стоимость в 513 627 рублей[5]. Однако в ходе строительства смета была существенно превышена и составила более 600 тысяч рублей.

Первый этаж здания был сдан в аренду под торговые помещения. Там продавались мануфактурные товары, велосипеды, граммофоны, обои, мраморные изделия, оптические принадлежности, оружие, кондитерские изделия[5]. Второй, третий и четвёртый этажи занимали городская дума и городская управа[4]. Городская дума была открыта для посещения. В зале для заседаний для публики отводилось 200 мест на хорах и 40 — в партере за барьером. Позднее за этим барьером установили стол для прессы[5].

В 1922 году в здании городской думы произошёл пожар, в результате чего были утрачены угловые купола. В годы Великой Отечественной войны здание подверглось ещё более серьёзным разрушениям, но вскоре после её окончания было восстановлено[4]. В здании размещался областной комитет КПСС.</Modal.Body>
                </Modal>
    </>    
  )
}


//<Container maxWidth="md-10">