import React, { Component } from 'react';
import { Col, Container, Nav, Row, Tab, TabContainer } from 'react-bootstrap';
import "../App.css";
import team from '../assets/team.jpg';

export default class About extends Component {
  render() {
    return (
      <section className="main-container">
        <Container>
          <TabContainer id="left-tabs-example" defaultActiveKey="first" >
            <Row>
                <Col sm={3}>
                      <Nav variant="pills" className="flex-column mt-2">
                          <Nav.Item>
                              <Nav.Link eventKey="first">Дизайн</Nav.Link>
                          </Nav.Item>
                          <Nav.Item>
                              <Nav.Link eventKey="second">Команда</Nav.Link>
                          </Nav.Item>
                          <Nav.Item>
                              <Nav.Link eventKey="third">Backend</Nav.Link>
                          </Nav.Item>
                          <Nav.Item>
                              <Nav.Link eventKey="fourth">Фреймворки</Nav.Link>
                          </Nav.Item>
                          <Nav.Item>
                              <Nav.Link eventKey="fifth">Библиотеки</Nav.Link>
                          </Nav.Item>
                      </Nav>
                </Col> 
                <Col sm={9}>
                  <Tab.Content className="mt-3">
                    <Tab.Pane eventKey="first">
                        <img src="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/89d45527-c11f-425e-96f3-c917f27e5ca9/figma-covers-in-action.gif"/>                      
                    </Tab.Pane>
                    <Tab.Pane eventKey="second">
                        <img src={team}/>

                    </Tab.Pane>
                    <Tab.Pane eventKey="third">
                        <img src="https://habrastorage.org/getpro/habr/upload_files/2d8/75d/986/2d875d9862cf98898687ca1b46786239.jpg"/>

                    </Tab.Pane>
                    <Tab.Pane eventKey="fourth">
                        <img src="https://thumbs.dreamstime.com/b/framework-%D1%81%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD-%D0%BA%D0%B0%D0%BA-%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BE%D0%BB%D0%BE%D0%BC%D0%BA%D0%B0-%D0%B8%D0%B7%D0%BE%D0%B1%D1%80%D0%B0%D0%B6%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9-word-%D0%BD%D0%B0-%D1%84%D1%80%D0%B0%D0%B3%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%85-164221084.jpg"/>

                    </Tab.Pane>
                    <Tab.Pane eventKey="fifth">
                        <img src="https://www.omeecron.com/wp-content/uploads/2022/06/reactjs.png"/>

                    </Tab.Pane>
                  </Tab.Content>
                </Col>
            </Row>
          </TabContainer>
        </Container> 
      </section>    
    )
  }
}
