import React, { Component } from 'react';
import { Container,  Navbar, Nav, Form, Button, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom';

import Login from '../Pages/Login';
import About from '../Pages/About';
import Attractions from '../Pages/Attractions';
import Pathways from '../Pages/Pathways';
import Guides from '../Pages/Guides';


//import { DialogContent, DialogTitle } from '@material-ui/core';

//import TextField from '@material-ui/core/TextField';
//import Dialog from '@material-ui/core/Dialog';
//import DialogActions from '@material-ui/core/DialogActions';



export default function Header() {

      return (       
            <>
            <Router>
                <Navbar collapseOnSellect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand >
                            RG
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-naw" />
                        <Navbar.Collapse id="responsive-navbar-naw">
                            <Nav className="me-auto">                                                        
                                <Nav.Link as={Link} to="/about"> Про нас </Nav.Link>
                                <Nav.Link as={Link} to="/attractions"> Достопримечательности </Nav.Link>
                                <Nav.Link as={Link} to="/pathways"> Маршруты </Nav.Link>
                                <Nav.Link as={Link} to="/guides"> Гиды </Nav.Link>
                            </Nav>  
                            <Form className='search-form'>
                                <Row>
                                    <Col>
                                        <Form.Control 
                                        id="inlineFormInput"
                                        placeholder="Search" />
                                        
                                    </Col>
                                </Row>
                            </Form>
                            <Button variant="outline-info">Search</Button>
                        <div className='ms-5'>
                            <Button as={Link} to="/login" className='me-2' > Вход </Button>                                 
                            <Button variant='primary'>Выйти</Button>
                        </div>
                                                    
                        </Navbar.Collapse>
                    </Container>
                    </Navbar>

                
                    <Routes>                    
                        <Route exact path="/about" element={<About/>} />
                        <Route exact path="/attractions" element={<Attractions/>} />
                        <Route exact path="/pathways" element={<Pathways/>} />                      
                        <Route exact path="/guides" element={<Guides/>} />
                        <Route exact path="/login" element={<Login/>} />                        
                    </Routes>
            </Router>
            </> 
    )
}

