import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import museum from '../assets/museum.jpg';
import museum2 from '../assets/museum2.jpg';
import tsum from '../assets/tsum.jpg';



export default class CarouselBox extends Component {
  render() {
    return (
        <Carousel>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={ museum }
                    alts="1"
                />
                <Carousel.Caption>
                    <h3>Ростовский областной музей краеведения</h3>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={ museum2 }
                    alts="2"
                />
                <Carousel.Caption>
                    <h3>Центральный парк культуры и отдыха имени М. Горького</h3>                   
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={ tsum }
                    alts="3"
                />
                <Carousel.Caption>
                    <h1>ЦУМ</h1>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>      
    )
  }
}
