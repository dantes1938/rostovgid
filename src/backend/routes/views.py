from django.shortcuts import render
import json
import os
import requests

with open("routes/data/routes.json", "r", encoding="utf-8") as f:
    routes = json.load(f)
routes = routes["Routes"]["items"]


def index(request):
    return render(request, "routes/index.html", {
        "routes": routes
    })


def route(request, id):
    route = routes[id]

    points = []

    for i in route["Points"]:
        try:
            lat, long = map(float, i.split(','))
            points.append([lat, long])
        except:
            r = requests.get(
                f'https://geocode-maps.yandex.ru/1.x/?format=json&apikey=d0c0fdfa-9ad7-4c7b-bc8c-d155a8326543&geocode=Ростов‑на‑Дону,{i}')
            r = r.json()
            long, lat = map(float, r["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"][
                "pos"].split(" "))
            points.append([lat, long])
        print(points)
    return render(request, "routes/route.html", {
        "route": route,
        "points": points
    })
