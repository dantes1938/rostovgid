import gpxpy
import gpxpy.gpx
import json

with open("routes.json","r") as f:
    routes = json.load(f)

route = {"Name": "Ростов - Волошинский пруд. Вдоль Щепки по трассе.","Description": "Ростов - Волошинский пруд. Вдоль Щепки по трассе.","Points":[],"id":int(routes["Routes"]["items"][-1]["id"]+1)}
points = []
with open('Yudino_vdol_Schepki.gpx', 'r',encoding='utf-8') as f:
    gpx = gpxpy.parse(f)
    for track in gpx.tracks:
        for segment in track.segments:
            i = 0
            while i < len(segment.points):
                points.append(str(segment.points[i].latitude) + ', ' + str(segment.points[i].longitude))
                i += 50
route["Points"] = points
routes["Routes"]["items"].append(route)


with open('routes.json', 'w', encoding='utf-8') as f:
    json.dump(routes, f,ensure_ascii=False)
print(route)

