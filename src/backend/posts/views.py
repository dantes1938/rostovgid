from django.shortcuts import render
from django.shortcuts import render
from django import forms
from django.http import HttpResponseRedirect
from django.urls import reverse
import json
import os
import requests


class NewSightForm(forms.Form):
    name = forms.CharField(label="Название достопримечательности")
    location = forms.CharField(label="Адрес достопримечательности")
    description = forms.CharField(label="Описание достопримечательности", widget=forms.Textarea)


with open("posts/data/Sights.json", "r", encoding="utf-8") as f:
    posts = json.load(f)
posts = posts["Sights"]["items"]


def index(request):
    return render(request, "posts/index.html", {
        "posts": posts
    })


def sight(request, id):
    sight = posts[id]
    images = []
    try:
        for file in os.listdir(f'posts/static/Photos/{posts[id]["Name"]}/'):
            if ".jpg" in file.lower():
                images.append(f'/Photos/{posts[id]["Name"]}/{file}')
    except:
        pass

    try:
        lat, long = map(float, sight["Location"].split(','))
    except:
        r = requests.get(
            f'https://geocode-maps.yandex.ru/1.x/?format=json&apikey=d0c0fdfa-9ad7-4c7b-bc8c-d155a8326543&geocode=Ростов‑на‑Дону,{sight["Location"]}')
        r = r.json()
        long, lat = map(float,
                        r["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"].split(
                            " "))

    return render(request, "posts/sight.html", {
        "sight": sight,
        "images": images,
        "lat": lat,
        "long": long
    })


def add(request):
    if request.method == "POST":
        form = NewSightForm(request.POST)
        if form.is_valid():
            with open("posts/data/added_sights.json", "r") as f:
                sights = json.load(f)
            new_sight = {"Name": form.cleaned_data["name"], "Location": form.cleaned_data["location"],
                         "Description": form.cleaned_data["description"]}
            sights["Sights"]["items"].append(new_sight)
            with open('posts/data/added_sights.json', 'w', encoding='utf-8') as f:
                json.dump(sights, f)
            return HttpResponseRedirect(reverse("posts:index"))
        else:
            return render(request, "posts/add.html", {
                "form": form
            })

    return render(request, "posts/add.html", {
        "form": NewSightForm()
    })
