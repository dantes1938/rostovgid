from django.urls import path, include
from . import views
from registration.views import Register
from django.views.generic import TemplateView
app_name = 'registration'
urlpatterns = [
    path('auth/', include('django.contrib.auth.urls')),
    path('register/', Register.as_view(), name="register"),

]
