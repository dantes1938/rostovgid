from django.shortcuts import render
from django import forms
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
import json
import os
import requests
from datetime import datetime
import sqlite3
from django.views.generic import View


class NewEventForm(forms.Form):
    name = forms.CharField(label="Название достопримечательности")
    date = forms.CharField(label="Дата встречи")
    type = forms.CharField(label="Тип встречи(открытая/закрытая")


def index(request):
    username = request.user.username
    my_events = {"open": [], "close": []}
    other_events = {"open": [], "close": []}
    with_me = []

    db = sqlite3.connect('db.sqlite3')
    c = db.cursor()
    c.execute("""SELECT * FROM events
    """)
    db.commit()
    for i in c.fetchall():
        event = {"Name": i[2], "Organisator": i[3], "Date": i[4],
                 "People": [x.strip(' ') for x in i[5].split(" ") if x]}
        if i[1] == 'open':
            if i[3] == username:
                my_events["open"].append(event)
            elif username in i[5].split(" "):
                with_me.append(event)
            else:
                other_events["open"].append(event)
        elif i[1] == 'close':
            if i[3] == username:
                my_events["close"].append(event)
                event["To_accept"] = [x.strip(' ') for x in i[6].split(" ") if x]
                print(event["To_accept"])
            elif username in i[5].split(" "):
                with_me.append(event)
            else:
                other_events["close"].append(event)

    db.close()

    return render(request, "events/index.html", {
        "my_events": my_events,
        "other_events": other_events,
        "with_me": with_me,
    })


def add(request):
    username = request.user.username
    if request.method == "POST":
        form = NewEventForm(request.POST)
        if form.is_valid():
            new_event = {"Organisator": username, "Name": form.cleaned_data["name"], "Date": form.cleaned_data["date"],
                         "People": []}
            if str(form.cleaned_data["type"]).upper() == "ОТКРЫТАЯ":
                db = sqlite3.connect('db.sqlite3')
                c = db.cursor()
                c.execute(
                    """INSERT INTO events (type,name,organisator,date,people,to_cofirm) 
                    VALUES ('open',?,?,?,'','')""",
                    (new_event["Name"], new_event["Organisator"], new_event["Date"]))
                db.commit()
                db.close()
            elif str(form.cleaned_data["type"]).upper() == "ЗАКРЫТАЯ":
                db = sqlite3.connect('db.sqlite3')
                c = db.cursor()
                c.execute(
                    """INSERT INTO events (type,name,organisator,date,people,to_cofirm) 
                    VALUES ('close',?,?,?,'','')""",
                    (new_event["Name"], new_event["Organisator"], new_event["Date"]))
                db.commit()
                db.close()

            return HttpResponseRedirect(reverse("events:index"))
        else:
            return render(request, "events/add.html", {
                "form": form
            })

    return render(request, "events/add.html", {
        "form": NewEventForm()
    })


class AjaxHandler(View):
    def get(self, request):
        type = request.headers.get('type')
        user = request.headers.get('user')
        date = str(request.headers.get('d'))
        organisator = request.headers.get('organisator')
        result = {}
        if type == "open" or type == "close":
            db = sqlite3.connect('db.sqlite3')
            c = db.cursor()
            c.execute("""SELECT * FROM events 
            WHERE date = ? AND organisator = ? AND type = ?""",
                      (date, organisator, type))
            event = c.fetchone()
            if type == 'open':
                people = [x.strip(' ') for x in event[5].split(" ") if x]
                if user not in people:
                    people.append(user)
                    people = " ".join(people)
                    c.execute("""UPDATE events SET people = ? 
                    WHERE date = ? AND organisator = ? AND type = ?""",
                              (people, date, organisator, type))

            elif type == 'close':
                people = [x.strip(' ') for x in event[6].split(" ") if x]
                if user not in people:
                    people.append(user)
                    people = " ".join(people)
                    c.execute("""UPDATE events SET to_cofirm = ? 
                    WHERE date = ? AND organisator = ? AND type = ?""",
                              (people, date, organisator, type))

            elif type == 'accept':
                people1 = [x.strip(' ') for x in event[5].split(" ") if x]
                people2 = [x.strip(' ') for x in event[6].split(" ") if x]
                if user not in people1 and user in people2:
                    people1.append(user)
                    people2.remove(user)
                    people1 = " ".join(people1)
                    people2 = " ".join(people2)
                    c.execute("""UPDATE events SET people = ?,to_cofirm = ?
                    WHERE date = ? AND organisator = ? AND type = ?""",
                              (people1, people1, date, organisator, type))
            db.commit()
            db.close()

        return JsonResponse(result)
