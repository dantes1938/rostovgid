document.addEventListener("DOMContentLoaded", function() {


            document.querySelectorAll('#join').forEach((button) =>{
                button.onclick =  async function () {
                        const csrf = document.querySelector('[name="csrfmiddlewaretoken"]').value;
                        let response = await fetch('/events/join', {
                            method: 'post',
                            headers: {
                                'X-CSRFToken': csrf,
                                'type': button.dataset.type,
                                'user': button.dataset.user,
                                'd': button.dataset.date,
                                'organisator': button.dataset.org,
                            }

                        })

                        let data = await response.json()
                        location.reload()


                }
            })



});

