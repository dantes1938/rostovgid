from django.urls import path
from . import views
from .views import AjaxHandler
app_name = 'events'

urlpatterns = [
    path("",views.index,name="index"),
    path("add", views.add,name="add"),
    path("join",AjaxHandler.as_view())
]