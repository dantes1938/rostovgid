from django.shortcuts import render
import sqlite3
from django import forms
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse


# Create your views here.

class NewGuideForm(forms.Form):
    name = forms.CharField(label="Имя")
    phone = forms.CharField(label="Номер телефона")


def index(request):
    guides = []
    db = sqlite3.connect('db.sqlite3')
    c = db.cursor()
    c.execute("""SELECT * FROM guides
        """)
    db.commit()
    for i in c.fetchall():
        guide = {"Name": i[1], "Phone": i[2]}
        guides.append(guide)
    db.close()

    return render(request, 'guides/index.html', {
        "guides": guides,
    })


def add(request):
    username = request.user.username
    if request.method == "POST":
        form = NewGuideForm(request.POST)
        if form.is_valid():
            new_guide = {"Name": form.cleaned_data["name"], "Phone": form.cleaned_data["phone"]}
            db = sqlite3.connect('db.sqlite3')
            c = db.cursor()
            c.execute(
                """INSERT INTO guides (name,phone) 
            VALUES (?,?)""",
                (new_guide["Name"], new_guide["Phone"]))
            db.commit()
            db.close()
            return HttpResponseRedirect(reverse("guides:index"))
        else:
            return render(request, "guides/add.html", {
                "form": form
            })

    return render(request, "guides/add.html", {
        "form": NewGuideForm()
    })
